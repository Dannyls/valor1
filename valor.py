# ___Programa para calcular el valor de una expresión
# ___Author___ = "Danny Lima"
# ___Email___ = "danny.lima@unl.edu.ec

ancho = 17
alto = 12.0
exp1 = ancho/2
print(f"El valor de la  primera expresion es igual a :    {exp1}   de tipo float ")
exp2 = ancho/2.0
print(f"El valor de la  segunda  expresion es igual a :    {exp2}   de tipo float ")
exp3 = alto/3
print(f"El valor de la  tercera  expresion es igual a :    {exp3}   de tipo float ")
exp4 = 1+2*5
print(f"El valor de la  cuarta  expresion es igual a :    {exp4}   de tipo int ")